//
// Bitmovin Player iOS SDK
// Copyright (C) 2021, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

/** The format of a subtitle file */
typedef NS_ENUM(NSUInteger, BMPSubtitleFormat) {
    BMPSubtitleFormatWebVtt,
    BMPSubtitleFormatTtml
} NS_SWIFT_NAME(SubtitleFormat);
