//
// Bitmovin Player iOS SDK
// Copyright (C) 2020, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import "_BMPTimelineMarker.h"

@class _BMPTimeline;

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(_TimelineDelegate)
@protocol _BMPTimelineDelegate <NSObject>
/**
 Gets called once the time line enters the according BMPTimelineMarker's time range.
 @param timeline The time line which is calling the delegate.
 @param marker The marker which's time range got entered.
 */
- (void)timeline:(_BMPTimeline *)timeline didEnterTimelineMarker:(id<_BMPTimelineMarker>)marker;

/**
Gets called once the time line exits the according BMPTimelineMarker's time range.
@param timeline The time line which is calling the delegate.
@param marker The marker which's time range got exited.
*/
- (void)timeline:(_BMPTimeline *)timeline didExitTimelineMarker:(id<_BMPTimelineMarker>)marker;

@end

NS_ASSUME_NONNULL_END
