//
// Bitmovin Player iOS SDK
// Copyright (C) 2018, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BMPConfig.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(UserInterfaceConfig)
@interface BMPUserInterfaceConfig : BMPConfig
@end

NS_ASSUME_NONNULL_END
