//
// Bitmovin Player iOS SDK
// Copyright (C) 2020, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BMPCue.h>
#import "_BMPTimelineMarker.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(_CueTimelineMarker)
@interface _BMPCueTimelineMarker : NSObject <_BMPTimelineMarker>
@property (nonatomic, strong, readonly, nonnull) BMPCue *cue;
- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;
- (instancetype)initWithCue:(BMPCue *)cue NS_DESIGNATED_INITIALIZER;
@end

NS_ASSUME_NONNULL_END
