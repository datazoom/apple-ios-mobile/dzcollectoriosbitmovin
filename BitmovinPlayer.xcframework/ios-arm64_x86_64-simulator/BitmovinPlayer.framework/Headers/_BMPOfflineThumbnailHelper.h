//
// Bitmovin Player iOS SDK
// Copyright (C) 2018, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BMPThumbnail.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(_OfflineThumbnailHelper)
@interface _BMPOfflineThumbnailHelper : NSObject
/**
 Prepares an array of thumbnails for offline usage, i.e. loads the local thumbnail image and encodes the content as base64 data Uri.
 The result is then set as the thumbnail's URL.

 @param thumbnails The thumbnails to use for offline usage.
 @return An NSArray of BMPThumbnails with their corresponding data Uris set.
 */
- (NSArray<BMPThumbnail *> *)prepareThumbnailsForOfflineUsage:(NSArray<BMPThumbnail *> *)thumbnails;
/**
 Changes the currently set URLs of the given thumbnails to the URL of the corresponding offline image.

 @param urls The dictionary containing the mapping from original URL strings to local URLs.
 @param thumbnails The thumbnails whose URLs should be swapped.
 @return An NSArray of BMPThumbnails with their URLs set to the according local image URL.
 */
- (NSArray<BMPThumbnail *> *)swapUrls:(NSDictionary<NSString *, NSURL *> *)urls forThumbnails:(NSArray<BMPThumbnail *> *)thumbnails;
/**
 Fetches a dictionary of distinct image URLs and their according local file URL (relative to directory) for a given array of thumbnails.

 @param thumbnails The thumbnails to fetch the image URLs from.
 @param directory The offline thumbnails directory where the images will be stored.
 @return A NSDictionary containing a mapping of distinct image URLs to there local equivalent.
 */
- (NSDictionary<NSString *, NSURL *> *)distinctImageUrlsAndTheirLocalEquivalentForThumbnails:(NSArray<BMPThumbnail *> *)thumbnails offlineDirectory:(NSURL *)directory;
@end

NS_ASSUME_NONNULL_END
