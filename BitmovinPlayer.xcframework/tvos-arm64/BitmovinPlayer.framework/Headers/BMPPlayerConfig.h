//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BMPConfig.h>
#import <BitmovinPlayer/BMPSourceConfig.h>
#import <BitmovinPlayer/BMPAdvertisingConfig.h>

@class BMPStyleConfig;
@class BMPLiveConfig;
@class BMPBufferConfig;
@class BMPPlaybackConfig;
@class BMPNetworkConfig;
@class BMPRemoteControlConfig;
@class BMPAdaptationConfig;
@class BMPTweaksConfig;

NS_ASSUME_NONNULL_BEGIN

/**
 * Configures a new `Player` instance.
 * Must not be modified after it was used to create a `Player` instance.
 */
NS_SWIFT_NAME(PlayerConfig)
@interface BMPPlayerConfig : BMPConfig
/**
 * A Bitmovin license key that can be found in the Bitmovin portal. If a license key is set here, it will be used
 * instead of the license key found in the Info.plist.
 */
@property (nonatomic, nullable, copy) NSString *key;
/**
 * Configures visual presentation and behaviour of the Player UI.
 * A default `StyleConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPStyleConfig *styleConfig;
/**
 * Configures playback behaviour.
 * A default `PlaybackConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPPlaybackConfig *playbackConfig;
/**
 * Configures advertising functionality.
 * A default `PlaybackConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPAdvertisingConfig *advertisingConfig __TVOS_PROHIBITED;
/**
 * Configures remote playback functionality.
 * A default `RemoteControlConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPRemoteControlConfig *remoteControlConfig __TVOS_PROHIBITED;
/**
 * Configures adaptation logic.
 * A default `AdaptationConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPAdaptationConfig *adaptationConfig;
/**
 * Configures network request manipulation functionality.
 * A default `NetworkConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPNetworkConfig *networkConfig;
/**
 * Configures experimental features.
 * A default `TweaksConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPTweaksConfig *tweaksConfig;
/**
 * Configures buffer settings.
 * A default `BufferConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPBufferConfig *bufferConfig API_AVAILABLE(ios(10.0), tvos(10.0));
/**
 * Configures behaviour when playing live content.
 * A default `LiveConfig` is set initially.
 */
@property (nonatomic, nonnull, strong) BMPLiveConfig *liveConfig;
@end

NS_ASSUME_NONNULL_END
