//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/_BMPJsonEncodable.h>
#import <BitmovinPlayer/_BMPJsonDecodable.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Provides methods to serialize and deserialize objects to JSON strings.
 */
@protocol _BMPJsonable <NSObject, _BMPJsonEncodable, _BMPJsonDecodable>
@end

NS_ASSUME_NONNULL_END
