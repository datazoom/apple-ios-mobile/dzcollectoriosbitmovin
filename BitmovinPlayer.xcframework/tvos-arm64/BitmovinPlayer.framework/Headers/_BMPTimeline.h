//
// Bitmovin Player iOS SDK
// Copyright (C) 2020, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import "_BMPTimelineMarker.h"
#import "_BMPTimelineDelegate.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(_Timeline)
@interface _BMPTimeline : NSObject
/**
 The delegate for this timeline. According methods will always be called on the main thread.
 */
@property (nonatomic, weak) id<_BMPTimelineDelegate> delegate;
/**
 A call to this method signals a time update to the timeline.
 On every time update the timeline will check for newly entered or exited markers and call the delegate accordingly.

 If progressive if set to YES, all delegate calls related to markers, that would have been skipped by the provided time update,
 will also be delivered in according order.

 @param playbackTime The updated playback time to singnal to the timeline.
 @param progressive A flag that indicates if the time update should be handled as a progressive (i.e. continuous) update.
 */
- (void)timeUpdate:(NSTimeInterval)playbackTime progressive:(BOOL)progressive;
/**
 Add a timeline maker to the timeline.
 */
- (void)addTimelineMarker:(id<_BMPTimelineMarker>)marker;
- (void)clearTimelineMarker;
- (void)resetLastPlaybackTime;

@end

NS_ASSUME_NONNULL_END
