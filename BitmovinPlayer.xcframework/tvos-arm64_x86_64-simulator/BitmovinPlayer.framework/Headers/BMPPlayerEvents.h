//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#ifndef BMPWebPlayerEvents_h
#define BMPWebPlayerEvents_h

#import <BitmovinPlayer/BMPEvent.h>
#import <BitmovinPlayer/BMPPlayerEvent.h>
#import <BitmovinPlayer/BMPVideoSizeChangedEvent.h>
#import <BitmovinPlayer/BMPCastAvailableEvent.h>
#import <BitmovinPlayer/BMPCastPausedEvent.h>
#import <BitmovinPlayer/BMPCastPlaybackFinishedEvent.h>
#import <BitmovinPlayer/BMPCastPlayingEvent.h>
#import <BitmovinPlayer/BMPCastStartedEvent.h>
#import <BitmovinPlayer/BMPCastStartEvent.h>
#import <BitmovinPlayer/BMPCastStoppedEvent.h>
#import <BitmovinPlayer/BMPCastTimeUpdatedEvent.h>
#import <BitmovinPlayer/BMPCastWaitingForDeviceEvent.h>
#import <BitmovinPlayer/BMPDvrWindowExceededEvent.h>
#import <BitmovinPlayer/BMPCueParsedEvent.h>
#import <BitmovinPlayer/BMPCueEnterEvent.h>
#import <BitmovinPlayer/BMPCueExitEvent.h>
#import <BitmovinPlayer/BMPCueEvent.h>
#import <BitmovinPlayer/BMPUnmutedEvent.h>
#import <BitmovinPlayer/BMPAdScheduledEvent.h>
#import <BitmovinPlayer/BMPVideoDownloadQualityChangedEvent.h>
#import <BitmovinPlayer/BMPControlsShowEvent.h>
#import <BitmovinPlayer/BMPControlsHideEvent.h>
#import <BitmovinPlayer/BMPAudioRemovedEvent.h>
#import <BitmovinPlayer/BMPAdQuartileEvent.h>

@class BMPReadyEvent;
@class BMPDestroyEvent;
@class BMPPlayEvent;
@class BMPAudioAddedEvent;
@class BMPDrmDataParsedEvent;
@class BMPDurationChangedEvent;
@class BMPDownloadFinishedEvent;
@class BMPTimeShiftEvent;
@class BMPTimeShiftedEvent;
@class BMPPlaybackFinishedEvent;
@class BMPSeekEvent;
@class BMPSeekedEvent;
@class BMPSourceUnloadedEvent;
@class BMPSourceLoadEvent;
@class BMPSourceLoadedEvent;
@class BMPSourceUnloadEvent;
@class BMPPlayerActiveEvent;
@class BMPPlayerInactiveEvent;
@class BMPPlaylistTransitionEvent;
@class BMPSourceMetadataChangedEvent;
@class BMPPlayerWarningEvent;
@class BMPSourceWarningEvent;
@class BMPPlayerErrorEvent;
@class BMPSourceErrorEvent;
@class BMPAdManifestLoadEvent;
@class BMPAdManifestLoadedEvent;
@class BMPAdErrorEvent;
@class BMPRenderFirstFrameEvent;
@class BMPStallEndedEvent;
@class BMPStallStartedEvent;
@class BMPPlayingEvent;
@class BMPPausedEvent;
@class BMPAudioChangedEvent;
@class BMPMutedEvent;
@class BMPFullscreenDisabledEvent;
@class BMPFullscreenEnabledEvent;
@class BMPFullscreenEnterEvent;
@class BMPFullscreenExitEvent;
@class BMPAirPlayChangedEvent;
@class BMPAirPlayAvailableEvent;
@class BMPMetadataEvent;
@class BMPMetadataParsedEvent;
@class BMPSubtitleRemovedEvent;
@class BMPSubtitleChangedEvent;
@class BMPSubtitleAddedEvent;
@class BMPAdFinishedEvent;
@class BMPAdSkippedEvent;
@class BMPAdStartedEvent;
@class BMPAdClickedEvent;
@class BMPAdBreakFinishedEvent;
@class BMPAdBreakStartedEvent;
@class BMPTimeChangedEvent;

#endif /* BMPWebPlayerEvents_h */
