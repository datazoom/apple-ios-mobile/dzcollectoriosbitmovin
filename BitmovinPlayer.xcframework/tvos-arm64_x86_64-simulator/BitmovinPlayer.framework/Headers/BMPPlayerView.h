//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//


#import <UIKit/UIKit.h>
#import <BitmovinPlayer/BMPPlayer.h>
#import <BitmovinPlayer/BMPPlayerView.h>
#import <BitmovinPlayer/BMPFullscreenHandler.h>
#import <BitmovinPlayer/BMPUserInterfaceApi.h>
#import <BitmovinPlayer/BMPUserInterfaceEventHandler.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * A view that provides the Bitmovin Player UI and default UI handling to an attached `Player` instance.
 * This view needs a `Player` instance to work properly. This Player can be passed to the initializer, or using the
 * according property if the view is created using the interface builder.
 *
 * @discussion
 * If you are composing the view via the interface builder make sure you use the obj-c class name `BMPPlayerView`
 *
 * If a custom UI is preferred, an `AVPlayerLayer` or an `AVPlayerViewController` can be registered via
 *  `registerPlayerLayer` / `registerPlayerViewController`. See `PlayerView` for more details.
 * @code
        // Create a subclass of UIView
        class CustomView: UIView {
            init(player: Player, frame: CGRect) {
                super.init(frame: frame)

                // register the AVPlayerLayer of this view to the Player
                player.register(playerLayer)
            }

            var playerLayer: AVPlayerLayer {
                layer as! AVPlayerLayer
            }

            override class var layerClass: AnyClass {
                AVPlayerLayer.self
            }
        }
 * @endcode
 */
NS_SWIFT_NAME(PlayerView)
@interface BMPPlayerView : UIView <BMPUserInterfaceApi, BMPUserInterfaceEventHandler>
@property (nullable, nonatomic, strong) id<BMPPlayer> player;

/**
 * Get/set a fullscreen handler for this `PlayerView`. See the documentation of the `FullscreenHandler` for
 * more information.
 */
@property (nonatomic, weak) id<BMPFullscreenHandler> fullscreenHandler;
/// :nodoc:
- (instancetype)init NS_UNAVAILABLE;
/// :nodoc:
- (instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
/// :nodoc:
- (instancetype)initWithCoder:(NSCoder *)aDecoder NS_UNAVAILABLE;
/// :nodoc:
+ (instancetype)new NS_UNAVAILABLE;

/**
 * Creates a new instance of the `PlayerView`.
 *
 * @param player The `Player` instance which will be associated with this `PlayerView` instance
 * @param frame The `CGRect` which will be passed to the `UIView` initializer
 */
- (instancetype)initWithPlayer:(id<BMPPlayer>)player frame:(CGRect)frame;

/**
 * When the hosting App supports more `UIDeviceOrientation` this method should be called before the view will rotate.
 *
 * @note This can be implemented inside the `viewWillTransition` method.
 * @code
        override func viewWillTransition(
            to size: CGSize,
            with coordinator: UIViewControllerTransitionCoordinator
        ) {
            playerView.willRotate()
            coordinator.animate(
                alongsideTransition: { _ in
                    ...
                },
                completion: { _ in
                    playerView.didRotate()
                }
            )

            super.viewWillTransition(to: size, with: coordinator)
        }
 */
- (void)willRotate;
/**
 * When the hosting App supports more `UIDeviceOrientation` this method should be called after the view rotated.
 *
 * @note This can be implemented inside the `viewWillTransition` method.
 * @code
        override func viewWillTransition(
            to size: CGSize,
            with coordinator: UIViewControllerTransitionCoordinator
        ) {
            playerView.willRotate()
            coordinator.animate(
                alongsideTransition: { _ in
                    ...
                },
                completion: { _ in
                    playerView.didRotate()
                }
            )

            super.viewWillTransition(to: size, with: coordinator)
        }
 */
- (void)didRotate;
@end

NS_ASSUME_NONNULL_END
