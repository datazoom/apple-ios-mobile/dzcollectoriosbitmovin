//
//  NativetvOSConnector.swift
//  DataZoom
//
//  Created by Momcilo Stankovic 26/06/21.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import Foundation
import AVFoundation
import MediaPlayer
import AVKit
import UIKit

import DZCollectoriOSBase
import BitmovinPlayer

@objc open class DZBitmovinCollector: NSObject  {
    public static let shared = DZBitmovinCollector()
   
    //Flag For Flux Data
    public var fluxDataFlag = true
    
    //Flag For DataZoom Internal Automation Purpose
    public var flagForAutomationOnly = Bool()
    
    @objc open func initCollector(configID:String, url:String, onCompletion: @escaping (Bool,Error?) -> Void) {
        DZCollectoriOSBitmovin.shared.createCollector(configID: configID, url: url, onCompletion: onCompletion)
    }
    
//    @objc open func createPlayerWithUrl(videoUrl: String) -> AVPlayer {
//        return DZCollectoriOSBitmovin.shared.createPlayerWithUrl(videoUrl: videoUrl)
//    }
    
    @objc open func initPlayer(playerInstance:Player, playerControllerInstance: UIViewController, adTag: String?) {
        DZCollectoriOSBitmovin.shared.initPlayer(playerInstance: playerInstance, playerControllerInstance: playerControllerInstance, adTag: adTag)
    }
    
//    @objc open func createAndInitPlayer(playerControllerInstance: AVPlayerViewController, videoUrl: String, adTag: String?, onCompletion: ((Error?) -> Void)?){
//        DZCollectoriOSBitmovin.shared.createAndInitPlayer(playerControllerInstance: playerControllerInstance, videoUrl: videoUrl, adTag: adTag, onCompletion: { error in onCompletion?(error)
//        })
//    }
    
    
    // Tracking Authorisation
    @objc open func askForTrackingAuthorization() {
        DZCollectoriOSBitmovin.shared.askConsent()
    }
    
    
    //MARK:- CUSTOM EVENTS AND METADATA
    @objc open func customEvent(eventName: String?,metadata:[String:Any]?) {
        DZCollectoriOSBitmovin.shared.triggerCustomEvent(name: eventName, metadata: metadata)
    }
    
    //MARK:- CUSTOM METADATA
    @objc open func initCustomMetadata(_ playerMetadata:[String:Any]?, _ sessionMetadata:[String:Any]?, _ customMetadata:[String:Any]?) {
        DZCollectoriOSBitmovin.shared.initCustomMetadata(playerMetadata, sessionMetadata, customMetadata)
    }
    
    // Player custom metadata
    @objc open func getPlayerCustomMetadata() -> [String:Any]? {
        return DZCollectoriOSBitmovin.shared.getPlayerCustomMetadata()
    }
    
    @objc open func setPlayerCustomMetadata(_ playerMetadata:[String:Any]?) {
        DZCollectoriOSBitmovin.shared.setPlayerCustomMetadata(playerMetadata)
    }
    
    @objc open func clearPlayerCustomMetadata() {
        DZCollectoriOSBitmovin.shared.clearPlayerCustomMetadata()
    }
    
    // Session custom metadata
    @objc open func getSessionCustomMetadata() -> [String:Any]? {
        return DZCollectoriOSBitmovin.shared.getSessionCustomMetadata()
    }
    
    @objc open func setSessionCustomMetadata(_ sessionMetadata:[String:Any]?) {
        DZCollectoriOSBitmovin.shared.setSessionCustomMetadata(sessionMetadata)
    }
    
    @objc open func clearSessionCustomMetadata() {
        DZCollectoriOSBitmovin.shared.clearSessionCustomMetadata()
    }
    
    // Custom metadata
    @objc open func getCustomMetadata() -> [String:Any]? {
        return DZCollectoriOSBitmovin.shared.getCustomMetadata()
    }
    
    @objc open func setCustomMetadata(_ customMetadata:[String:Any]?) {
        DZCollectoriOSBitmovin.shared.setCustomMetadata(customMetadata)
    }
    
    @objc open func clearCustomMetadata() {
        DZCollectoriOSBitmovin.shared.clearCustomMetadata()
    }
    
}
