//
//  DZCollectoriOSBase.swift
//  DZCollectoriOSBase
//
//  Created by Vuk Simovic on 2.3.22..
//

import Foundation
import AVFoundation
import MediaPlayer
import AVKit
import Network
import UIKit
import AdSupport

import BitmovinPlayer
import DZCollectoriOSBase

#if canImport(AppTrackingTransparency)
import AppTrackingTransparency
#endif


#if canImport(GoogleInteractiveMediaAds)
import GoogleInteractiveMediaAds
#endif

@available(iOS 12.0, *)
open class DZCollectoriOSBitmovin: DZCollectoriOSBase {
    public static let shared = DZCollectoriOSBitmovin()
    
    public var videoPlayerInstance: Player!
    public var videoPlayerControllerInstance: UIViewController?

#if canImport(GoogleInteractiveMediaAds)
    var adsRequested: Bool = false
    var adsLoader: IMAAdsLoader! = IMAAdsLoader()
    var adDisplayContainer: IMAAdDisplayContainer?
    var adsManager: IMAAdsManager!
    var contentPlayhead: IMAAVPlayerContentPlayhead?
    var adBreakActive = false
#endif
    
    var startTime : Double = 0.0
    var viewStartTime : Int = 0
    var timeSinceStarted : Double = 0.0
    var milestonePercent : Double = 0.0
    var lastEventTime: Double = 0.0
    
    var timeSinceLastHeartbeat: Double = 0.0
    var timeSinceLastMilestone: Double = 0.0
    var timeSinceLastPause : Double = 0.0
    var timeSinceStallStart: Double = 0.0
    var timeSinceBufferStart: Double = 0.0
    var timeSinceLastRenditionChange: Double = 0.0
    var timeSinceLastSeekStart: Double = 0.0
    
    var heartbeatTime: Double = 0.0
    var milestoneTime: Double = 0.0
    var pauseTime: Double = 0.0
    var stallTime: Double = 0.0
    var bufferTime: Double = 0.0
    var renditionChangeTime: Double = 0.0
    var seekStartTime: Double = 0.0
    var seekEndTime: Double = 0.0
    var stallCount: Int = 0
    var bufferCount: Int = 0
    var stallDuration: Double = 0.0
    var bufferDuration: Double = 0.0
    
//    var adMilestoneTime: Double = 0.0
    var adStallTime: Double = 0.0
    var adBufferTime: Double = 0.0
    var adRenditionChangeTime: Double = 0.0
    var adSeekStartTime: Double = 0.0
    
    var timeSinceLastAdBreakStart: Double = 0.0
//    var timeSinceLastAdRequested: Double = 0.0
//    var timeSinceLastAdStarted: Double = 0.0
//    var timeSinceLastAdCompleted: Double = 0.0
    
    var stallAdd = false
    
    var errorCode = ""
    var errorMsg = ""
    var errorCount = 0
    var adsErrorCount = 0
    var contentErrorCount = 0
    
    var bufferStartSent = false
    var contentStalled = false
    
    
    
    //MARK:- Create Bitmovin Connector
    @objc open func createCollector(configID:String, url:String, onCompletion: @escaping (Bool,Error?) -> Void) {
        createBaseCollector(configID: configID, url: url) { (success, error) in
            if success == true {
                DZEventCollector.shared.player.playerName = "iOS Bitmovin Player"
                DZEventCollector.shared.player.playerVersion = UIDevice.current.systemVersion
                
                DZEventCollector.shared.resetCounters()
                
                self.fluxDataFlag = DZBitmovinCollector.shared.fluxDataFlag
                self.flagForAutomationOnly = DZBitmovinCollector.shared.flagForAutomationOnly
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.triggerEvent(eventType: .datazoomLoaded)
                }
                
                #if canImport(GoogleInteractiveMediaAds)
                self.adsLoader = IMAAdsLoader(settings: nil)
                self.adsLoader.delegate = self
                #endif
                
                onCompletion(true, nil)
            }
            else {
                print("%%%% Error on Create Collector %%%% \n \(error!.localizedDescription)")
                onCompletion(false, error)
            }
        }
    }
    
    //MARK:- Add Boundary Time Observer
//    func addBoundaryTimeObserver() {
//        var times = [NSValue]()
//        var currentTime = CMTime.zero
//        let assetValue = videoPlayerInstance.currentItem?.asset
//        let interval = CMTimeMultiplyByFloat64((assetValue?.duration)!, multiplier: 0.25)
//        while currentTime < (assetValue?.duration)! {
//            currentTime = currentTime + interval
//            times.append(NSValue(time:currentTime))
//        }
//        let mainQueue = DispatchQueue.main
//        if times.count > 0 {
//            timeObserverToken = videoPlayerInstance.addBoundaryTimeObserver(forTimes: times, queue: mainQueue, using: {
//                [weak self] in
//                print("%%%% addBoundaryTimeObserver - IsPlaying : %%%% \n \(self!.isPlaying)")
//            }) as AnyObject
//        }
//    }
//
//    //MARK:- Remove Periodic Time Observer
//    func removePeriodicTimeObserver() {
//        if let token = timeObserverToken {
//            videoPlayerInstance.removeTimeObserver(token)
//            timeObserverToken = nil
//        }
//        if let token = periodicObserverToken {
//            videoPlayerInstance.removeTimeObserver(token)
//            periodicObserverToken = nil
//        }
//    }
    
    //MARK:- Dealloc Observers
//    @objc private func deallocObservers(player: Player) {
//        player.removeObserver(self, forKeyPath: "rate")
//        removePeriodicTimeObserver()
//    }
    
    
    //MARK:- Stop Recording Events For Player
    @objc open func stopRecordingEventsForPlayer(playerInstance:AVPlayer) {
//        playerInstance.removeObserver(self, forKeyPath: "status")
//        playerInstance.currentItem?.removeObserver(self, forKeyPath: "timebase")
    }
    
    //MARK:-  Main method, which initialises the notification and acts as an observer for events on nativeplayer.
    @objc open func recordEventsForPlayer(playerInstance:Player) {
//        videoPlayerInstance = playerInstance
//        videoPlayerInstance.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "timebase", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "tracks", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "seekableTimeRanges", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "asset", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "itemStatus", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "playbackBufferEmpty", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "playbackBufferFull", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "playbackStalled", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "AVPlayerItemNewErrorLogEntryNotification", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "volume", options: NSKeyValueObservingOptions.new, context: nil)
//        videoPlayerInstance.currentItem?.addObserver(self, forKeyPath: "timedMetadata", options: NSKeyValueObservingOptions.new, context: nil)
//
//        NotificationCenter.default.addObserver(self,selector: #selector(handleAVPlayerAccess), name: NSNotification.Name.AVPlayerItemNewAccessLogEntry, object: nil)
//        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayerInstance.currentItem)
//
//        DispatchQueue.main.async {
//            self.milestoneTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.triggerMilestoneEvent), userInfo: nil, repeats: true)
//        }
//        //self.qTimer?.fire()
//        addPeriodicTimeObserver()
//        self.viewStartTime = Int (NSDate().timeIntervalSince1970)
    }
    
    deinit {
//        deallocObservers(player: videoPlayerInstance)
    }
}


// Player
extension DZCollectoriOSBitmovin {
    func resetTimes() {
        engagementStart = 0.0
        startTime = 0.0
        viewStartTime = 0
        timeSinceStarted = 0.0
        milestonePercent = 0.0
        timeSinceLastHeartbeat = 0.0
        timeSinceLastMilestone = 0.0
        timeSinceLastPause = 0.0
        timeSinceStallStart = 0.0
        timeSinceBufferStart = 0.0
        timeSinceLastRenditionChange = 0.0
        timeSinceLastSeekStart = 0.0
//        DZEventCollector.shared.details.metrics.playbackDurationAds = 0
//        DZEventCollector.shared.details.metrics.playbackDurationContent = 0
//        DZEventCollector.shared.details.metrics.playbackDurationTotal = 0
    }
    

    //    MARK:-  Method to create player with videoUrl
//    @objc open func createPlayerWithUrl(videoUrl: String) -> AVPlayer {
//        originalVideoURLString = videoUrl
//        print("Original link \(originalVideoURLString!)")
//
//        UserDefaults.standard.createUserRequestID()
//        let videoUrlWithCMCD = addCMCDToVideoUrl(videoUrl: videoUrl)
//        print("With CMCD \(videoUrlWithCMCD)")
//
//        let contentUrl = URL(string: videoUrlWithCMCD)
////        let contentUrl = URL(string: originalVideoURLString!)
//
//        let player = AVPlayer(url: contentUrl!)
//        return player
//    }
    
    
    //    MARK:-  Method to init player
    @objc open func initPlayer(playerInstance:Player, playerControllerInstance: UIViewController? = nil, adTag: String? = nil) {
        self.videoPlayerInstance = playerInstance
        self.videoPlayerControllerInstance = playerControllerInstance
        
        self.recordEventsForPlayer(playerInstance: playerInstance)
        self.resetTimes()
        
        triggerEvent(eventType: .mediaRequest)
        
        #if canImport(GoogleInteractiveMediaAds)
        adsRequested = false
        contentPlayhead = IMAAVPlayerContentPlayhead(avPlayer: playerInstance)
        if let pcInstance = videoPlayerControllerInstance {
            adDisplayContainer = IMAAdDisplayContainer(adContainer: pcInstance.view, viewController: pcInstance)
            if let _ = adTag {
                adTagURLString = adTag
            }
        }
        #endif
    }
    
//    @objc open func createAndInitPlayer(playerControllerInstance: UIViewController?, videoUrl: String, adTag: String? = nil, onCompletion: ((Error?) -> Void)?){
//        videoPlayerControllerInstance = playerControllerInstance
//        let player = DZBitmovinCollector.shared.createPlayerWithUrl(videoUrl: videoUrl)
//        videoPlayerInstance = player
//        videoPlayerControllerInstance!.player = player
//
//        DZBitmovinCollector.shared.initPlayer(playerInstance: player, playerControllerInstance: playerControllerInstance!, adTag: adTag)
//
//        onCompletion?(nil)
//    }
}


@available(iOS 12.0, *)
extension DZCollectoriOSBitmovin : DZCollectorProtocol {
    func resetCommonAdEventData() {
        DZEventCollector.shared.video.mediaType = .none
        DZEventCollector.shared.adData.adSessionId = ""
        DZEventCollector.shared.adData.adBrakeId = ""
    }
    
    func eventCommonSettings(){
        // common
        let diff = (self.lastEventTime > 0.0) ? (CACurrentMediaTime()*1000 - lastEventTime) : 0.0
        print("Event timestamp difference \(diff)")
        if diff > (DZCollectoriOSBase.appSessionTimeout * 1000) {
            UserDefaults.standard.createUserSessionID()
            onResetAppSessionID()
        }
        self.lastEventTime = CACurrentMediaTime()*1000
        
        // content
        self.timeSinceStarted = (CACurrentMediaTime()*1000) - self.startTime
        self.timeSinceLastHeartbeat = (CACurrentMediaTime()*1000) - self.heartbeatTime
        self.timeSinceLastMilestone = (CACurrentMediaTime()*1000) - self.milestoneTime
        self.timeSinceLastPause = (self.pauseTime > 0.0) ? (CACurrentMediaTime()*1000) - self.pauseTime : 0.0
        self.timeSinceStallStart = (self.stallTime > 0.0) ? (CACurrentMediaTime()*1000) - self.stallTime : 0.0
        self.timeSinceBufferStart = (CACurrentMediaTime()*1000) - self.stallTime
        
        self.timeSinceLastRenditionChange = (self.renditionChangeTime > 0.0) ? (CACurrentMediaTime()*1000) - self.renditionChangeTime : 0.0
        self.timeSinceLastSeekStart = (self.seekStartTime > 0.0) ? (CACurrentMediaTime()*1000) - self.seekStartTime : 0.0
                
        // ad
        let timeSinceLastAdRequested = (DZEventCollector.shared.adRequestTime > 0.0) ? (CACurrentMediaTime()*1000) - DZEventCollector.shared.adRequestTime : 0.0
        let timeSinceLastAdStarted = (DZEventCollector.shared.adStartTime > 0.0) ? (CACurrentMediaTime()*1000) - DZEventCollector.shared.adStartTime : 0.0
        let timeSinceLastAdCompleted = (DZEventCollector.shared.adCompletedTime > 0.0) ? (CACurrentMediaTime()*1000) - DZEventCollector.shared.adCompletedTime : 0.0
        let timeSinceLastMilestoneAd = (DZEventCollector.shared.adMilestoneTime > 0.0) ? (CACurrentMediaTime()*1000) - DZEventCollector.shared.adMilestoneTime : 0.0
        
//        self.timeSinceLastAdBreakStart = (DZEventCollector.shared.adStartTime > 0.0) ? (CACurrentMediaTime()*1000) - DZEventCollector.shared.adStartTime + 21 : 0.0
        //self.timeSinceLastAdBreakStart = Int(((CFAbsoluteTimeGetCurrent() - adStartTime) * 1000) + 21)
    
        // common
        DZEventCollector.shared.viewStartTime = self.viewStartTime
        DZEventCollector.shared.contentStartTime = self.startTime
        DZEventCollector.shared.errorCount = errorCount
        
        DZEventCollector.shared.network.connectionType = connectionType
        
        DZEventCollector.shared.cdn.cdn = cdn
        
        DZEventCollector.shared.cmcd.sessionId = ""
        DZEventCollector.shared.cmcd.requestId = ""
        
        DZEventCollector.shared.userDetails.appSessionId = UserDefaults.standard.getUserSessionID()
        DZEventCollector.shared.userDetails.contentSessionId = UserDefaults.standard.getUserContentSessionID()
    
        DZEventCollector.shared.player.autostart = true
        DZEventCollector.shared.player.controls = false
        DZEventCollector.shared.player.defaultPlaybackRate = 1.0
        DZEventCollector.shared.player.loop = false
        DZEventCollector.shared.player.fullscreen = true
        DZEventCollector.shared.player.readyState = "1"
        
        DZEventCollector.shared.playerMetadata = playerMetadata
        DZEventCollector.shared.sessionMetadata = sessionMetadata
        DZEventCollector.shared.customMetadata = customMetadata
        
        DZEventCollector.shared.details.attributes.error.message = errorMsg
        DZEventCollector.shared.details.attributes.error.code = errorCode
        DZEventCollector.shared.details.metrics.numberErrors = errorCount
        
        DZEventCollector.shared.details.metrics.numberErrorsContent = contentErrorCount
        DZEventCollector.shared.details.metrics.numberErrorsAds = adsErrorCount
        
        DZEventCollector.shared.details.metrics.numberRequestingContent = self.contentRequestingCount

        DZEventCollector.shared.details.metrics.viewStartTimestamp = self.viewStartTime
        DZEventCollector.shared.details.metrics.timeSinceStarted = Int(self.timeSinceStarted)
        DZEventCollector.shared.details.metrics.timeSinceStartedContent = Int(self.timeSinceStarted)
        DZEventCollector.shared.details.metrics.timeSinceLastPause = Int(self.timeSinceLastPause)
        
        DZEventCollector.shared.details.metrics.timeSinceLastRequestAd = Int(timeSinceLastAdRequested)
        DZEventCollector.shared.details.metrics.timeSinceLastStartedAd = Int(timeSinceLastAdStarted)
        DZEventCollector.shared.details.metrics.timeSinceLastAdCompleted = Int(timeSinceLastAdCompleted)
        DZEventCollector.shared.details.metrics.timeSinceLastMilestoneAd = Int(timeSinceLastMilestoneAd)
        
        DZEventCollector.shared.details.metrics.timeSinceLastAdBreakStart = Int(self.timeSinceLastAdBreakStart)
        
        DZEventCollector.shared.details.metrics.timeSinceLastRenditionChange = Int(self.timeSinceLastRenditionChange)
    }
    
    // Trigger Event
    func triggerEvent(eventType: EventType, rateValue: Float = 0) {
        self.eventCommonSettings()
        
        let actualPresentTime : Float64  = Float64(0)
        let playHeadPos = Float(0)
//        let actualPresentTime : Float64  = (self.videoPlayerInstance != nil) ? self.videoPlayerInstance.currentTime().seconds : Float64(0)
//        let playHeadPos = (self.videoPlayerInstance != nil) ? Float(CMTimeGetSeconds(self.videoPlayerInstance.currentTime())) : Float(0)
//
//        let volumeValue = (self.videoPlayerInstance != nil) ? self.videoPlayerInstance.volume : 0
//        let playbackSpeed = (self.videoPlayerInstance != nil) ? self.videoPlayerInstance.rate : 0
//        let playerItem = self.videoPlayerInstance?.currentItem
//
//        let bitRate = playerItem?.preferredPeakBitRate
//        let urlAsset = playerItem?.asset as? AVURLAsset
//        //let contentUrl: URL? = urlAsset?.url
//        let contentSourceUrl: String? = self.originalVideoURLString //contentUrl?.absoluteString
//
//        let duration = urlAsset?.duration ?? CMTimeMake(value: 99, timescale: 1)
//        let videoTracks = urlAsset?.tracks(withMediaType: .video)
//        let fps = videoTracks?.first?.nominalFrameRate ?? 24
//
//        var subtitle = ""
//        if let group = playerItem?.asset.mediaSelectionGroup(forMediaCharacteristic: AVMediaCharacteristic.legible){
//            let selectedOption = playerItem?.currentMediaSelection.selectedMediaOption(in: group)
//            if let languageCode = selectedOption?.locale?.languageCode {
//                let locale: Locale = .current
//                subtitle = locale.localizedString(forLanguageCode: languageCode) ?? ""
//            }
//        }
//
//        // Check url - not robust enough
//        let streamingProtocol = urlAsset?.url.streamingProtocol()
//        let streamingType = urlAsset?.url.streamingType()
//        let title  = urlAsset?.url.lastPathComponent

        DZEventCollector.shared.video.mediaType = .content
        DZEventCollector.shared.video.frameRate = 0//fps
        DZEventCollector.shared.video.title = "" //title ?? ""
        DZEventCollector.shared.video.source = "" //contentSourceUrl ?? ""
        DZEventCollector.shared.video.duration = 0.0 //Float(CMTimeGetSeconds(duration))

        DZEventCollector.shared.player.streamingProtocol = "" //streamingProtocol ?? "" // Other
        DZEventCollector.shared.player.streamingType = "" //streamingType ?? "" // Undefine

        DZEventCollector.shared.details.metrics.frameRate = 0 //fps

        DZEventCollector.shared.details.metrics.renditionWidth = self.renditionWidth ?? Int(0)
        DZEventCollector.shared.details.metrics.renditionHeight = self.renditionHeight ?? Int(0)
        DZEventCollector.shared.details.metrics.renditionName = "\(self.renditionHeight ?? 0)p"
        DZEventCollector.shared.details.attributes.absShift = self.absShift
        DZEventCollector.shared.details.metrics.renditionVideoBitrate = Int(self.renditionVideoBitrate ?? 0)

        DZEventCollector.shared.details.metrics.currentSubtitles = "" //subtitle
        
        //DZEventCollector.shared.details.metrics.contentSessionStartTimestamp = Int(self.startTime)
        DZEventCollector.shared.details.metrics.engagementDuration = Int ((CACurrentMediaTime() - engagementStart) * 1000)
//            DZEventCollector.shared.details.metrics.playbackDuration = Int(playHeadPos)
//            DZEventCollector.shared.details.metrics.playerVolume = Int(volumeValue)
//            DZEventCollector.shared.details.metrics.playerBitRate = Float(bitRate ?? 99)
//            DZEventCollector.shared.details.metrics.playbackRate = playbackSpeed
//            DZEventCollector.shared.details.metrics.playerVolume = Int(volumeValue)
//            DZEventCollector.shared.details.metrics.playerBitRate = Float(bitRate ?? 99)
//            DZEventCollector.shared.details.metrics.playerItemDuration = Float(CMTimeGetSeconds(duration))

        if self.heartbeatTime > 0.0 {
            DZEventCollector.shared.details.metrics.timeSinceLastHeartbeat = Int(self.timeSinceLastHeartbeat )
        }
        if self.milestoneTime > 0.0 {
            DZEventCollector.shared.details.metrics.timeSinceLastMilestone = Int(self.timeSinceLastMilestone )
            DZEventCollector.shared.details.metrics.timeSinceLastMilestoneContent = Int(self.timeSinceLastMilestone)
        }
        
        if self.stallTime > 0.0 {
            DZEventCollector.shared.details.metrics.timeSinceStallStart = Int(self.timeSinceStallStart)
            DZEventCollector.shared.details.metrics.timeSinceLastStallStartContent = Int(self.timeSinceStallStart)
            DZEventCollector.shared.details.metrics.timeSinceStallStart = Int(self.timeSinceStallStart)
            DZEventCollector.shared.details.metrics.timeSinceBufferStart = Int(self.timeSinceStallStart)
            DZEventCollector.shared.details.metrics.timeSinceLastBufferStartContent = Int(self.timeSinceStallStart)
        }
        
        DZEventCollector.shared.details.metrics.stallCount = self.stallCount
        DZEventCollector.shared.details.metrics.stallDuration = Int(self.stallDuration)
        DZEventCollector.shared.details.metrics.bufferDuration = Int(self.bufferDuration)
        DZEventCollector.shared.details.metrics.bufferDurationContent = Int(self.bufferDuration)
        
        DZEventCollector.shared.details.metrics.timeSinceLastSeekStart = Int(self.timeSinceLastSeekStart)
        
        
        switch eventType {
        // Content Events

        case .datazoomLoaded:
            triggerDatazoomLoaded(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .error:
            triggerError(playHeadPos, {
                self.errorCount += 1
                self.contentErrorCount += 1
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .milestone:
            triggerMilestone(playHeadPos, {
                DZEventCollector.shared.details.attributes.milestonePercent = self.milestonePercent
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .renditionChange :
            triggerRenditionChange(playHeadPos, {
                self.renditionChangeTime = (CACurrentMediaTime()*1000)
                self.timeSinceLastRenditionChange = 0
                DZEventCollector.shared.details.metrics.timeSinceLastRenditionChange = Int(0)
            }, {
                print("Sent \(eventType.key) Event")
            })

            
        case .mediaRequest:
            triggerMediaRequest({
                self.renditionChangeTime = 0.0
                self.timeSinceLastRenditionChange = 0
                DZEventCollector.shared.details.metrics.timeSinceLastRenditionChange = Int(0)
                
                DZEventCollector.shared.details.metrics.contentSessionStartTimestamp = Int(Date().getTimestamp())
                DZEventCollector.shared.contentRequestTime = CACurrentMediaTime() * 1000
                
                self.contentRequestingCount += 1
                DZEventCollector.shared.details.metrics.numberRequestingContent = self.contentRequestingCount
                
                self.stallCount = 0
                DZEventCollector.shared.details.metrics.stallCount = self.stallCount
                
                self.seekStartTime = 0
                self.seekEndTime = 0
                self.timeSinceLastSeekStart = 0
                DZEventCollector.shared.details.metrics.timeSinceSeekStart = 0
                DZEventCollector.shared.details.metrics.timeSinceLastSeekStart = 0
            },{ (event) in
                event.cmcd.sessionId = UserDefaults.standard.getUserSessionID()
                event.cmcd.requestId = UserDefaults.standard.getUserRequestID()
                
                UserDefaults.standard.createUserContentSessionID()
                event.userDetails.contentSessionId = UserDefaults.standard.getUserContentSessionID()
                
                self.onCreateContentSessionID()
            },{
                print("Sent \(eventType.key) Event")
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                self.triggerMediaLoaded(nil, {
                    print("Sent \(EventType.mediaLoaded.key) Event")
                })
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.triggerPlaybackReady(nil, {
                    print("Sent \(EventType.playbackReady.key) Event")
                })
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                self.triggerPlaybackStart({
                    DZEventCollector.shared.startupDurationContent = Int(CACurrentMediaTime() * 1000 - DZEventCollector.shared.contentRequestTime)
                    DZEventCollector.shared.startupDurationTotal += DZEventCollector.shared.startupDurationContent
                    
                    DZEventCollector.shared.details.attributes.startupDurationContent = DZEventCollector.shared.startupDurationContent
                    DZEventCollector.shared.details.attributes.startupDurationTotal = DZEventCollector.shared.startupDurationTotal
                },{
                    print("Sent \(EventType.playbackStart.key) Event")
                })
            }
            
        case .mediaLoaded:
            triggerMediaLoaded(nil, {
                print("Sent \(EventType.mediaLoaded.key) Event")
            })

        case .playbackReady:
            triggerPlaybackReady(nil, {
                print("Sent \(eventType.key) Event")
            })
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.triggerPlaybackStart({
                    DZEventCollector.shared.startupDurationContent = Int(CACurrentMediaTime() * 1000 - DZEventCollector.shared.contentRequestTime)
                    DZEventCollector.shared.startupDurationTotal += DZEventCollector.shared.startupDurationContent
                    
                    DZEventCollector.shared.details.attributes.startupDurationContent = DZEventCollector.shared.startupDurationContent
                    DZEventCollector.shared.details.attributes.startupDurationTotal = DZEventCollector.shared.startupDurationTotal
                },{
                    print("Sent \(EventType.playbackStart.key) Event")
                })
            }
            
        case .playbackStart:
            triggerPlaybackStart({
                DZEventCollector.shared.startupDurationContent = Int(CACurrentMediaTime() * 1000 - DZEventCollector.shared.contentRequestTime)
                DZEventCollector.shared.startupDurationTotal += DZEventCollector.shared.startupDurationContent
                
                DZEventCollector.shared.details.attributes.startupDurationContent = DZEventCollector.shared.startupDurationContent
                DZEventCollector.shared.details.attributes.startupDurationTotal = DZEventCollector.shared.startupDurationTotal
            },{
                print("Sent \(eventType.key) Event")
            })
            
        case .play:
            triggerPlay(playHeadPos, {
                self.startTime = CACurrentMediaTime() * 1000
                self.timeSinceStarted = 0
                
                //DZEventCollector.shared.details.metrics.contentSessionStartTimestamp = Int(self.startTime)
                DZEventCollector.shared.details.metrics.timeSinceStarted = Int(self.timeSinceStarted)
                DZEventCollector.shared.details.metrics.timeSinceStartedContent = Int(self.timeSinceStarted)
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .playing:
            triggerPlaying(playHeadPos, nil, {
                print("Sent \(eventType.key) Event")
            })
        
        case .seekStart:
            triggerSeekStart(playHeadPos, Int(self.seekStartTime), Int(self.seekEndTime), {
                self.seekStartTime = CACurrentMediaTime() * 1000
                self.timeSinceLastSeekStart = 0.0
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .seekEnd:
            triggerSeekEnd(playHeadPos, Int(self.seekStartTime), Int(self.seekEndTime), {
                self.seekEndTime = CACurrentMediaTime() * 1000
                self.timeSinceLastSeekStart = 0.0
                DZEventCollector.shared.details.attributes.seekStart = Int(self.seekStartTime)
                DZEventCollector.shared.details.attributes.seekEnd = Int(self.seekEndTime)
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .playbackComplete :
            triggerPlaybackComplete(playHeadPos, {
                self.seekStartTime = 0.0
                self.seekEndTime = 0.0
                self.timeSinceLastSeekStart = 0.0
            }, {
                UserDefaults.standard.clearUserContentSessionID()
                print("Sent \(eventType.key) Event")
            })
            
        case .heartbeat :
            triggerHeartbeat(playHeadPos, nil, {
                print("Sent \(eventType.key) Event")
            })

        case .bufferStart :
            triggerBufferStart({
                self.bufferCount += 1
                self.bufferTime = CACurrentMediaTime() * 1000
            }, {
                print("Sent \(eventType.key) Event")
            })

        case .bufferEnd :
            triggerBufferEnd({
                self.stallDuration += ((CACurrentMediaTime() * 1000) - self.stallTime)
                self.bufferDuration += ((CACurrentMediaTime() * 1000) - self.bufferTime)
                DZEventCollector.shared.details.metrics.stallDuration = Int(self.stallDuration)
                DZEventCollector.shared.details.metrics.bufferDuration = Int(self.bufferDuration)
                DZEventCollector.shared.details.metrics.bufferDurationContent = Int(self.bufferDuration)
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .stallStart :
            triggerStallStart({
                self.stallCount += 1
                self.stallTime = CACurrentMediaTime() * 1000
                DZEventCollector.shared.details.metrics.stallCount = self.stallCount
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .stallEnd :
            triggerStallEnd({
                self.stallDuration += ((CACurrentMediaTime() * 1000) - self.stallTime)
                DZEventCollector.shared.details.metrics.stallDuration = Int(self.stallDuration)
            }, {
                print("Sent \(eventType.key) Event")
            })
            
        case .pause :
            self.pauseTime = (CACurrentMediaTime() * 1000)
            DZEventCollector.shared.details.metrics.timeSinceLastPause = 0
//            DZEventCollector.shared.details.metrics.timeSinceStartedContent = 0
            triggerPause(playHeadPos, nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .resume :
            let actualPresentTime : Float64  = Float64(0) //self.videoPlayerInstance.currentTime().seconds
//            presentTime = self.videoPlayerInstance.currentTime().seconds
//            presentTime = presentTime + 0.3
//
//            let forwardValue = presentTime - prevTime
//            let backwardValue =  prevTime - presentTime
//
//            if forwardValue > 0.4 || backwardValue > 0.4 {
//                print("seek forward")
//                triggerSeekStart(rateValue, Int(self.seekStartTime), Int(self.seekEndTime), {
//                    self.seekStartTime = CACurrentMediaTime() * 1000
//                    self.seekEndTime = 0.0
//                    self.timeSinceLastSeekStart = 0.0
//                }, {
//                    print("Sent \(eventType.key) Event")
//                })
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                    self.triggerSeekEnd(rateValue, Int(self.seekStartTime), Int(self.seekEndTime), {
//                        self.seekEndTime = CACurrentMediaTime() * 1000
//                        self.timeSinceLastSeekStart = 0.0
//                        DZEventCollector.shared.details.attributes.seekStart = Int(self.seekStartTime)
//                        DZEventCollector.shared.details.attributes.seekEnd = Int(self.seekEndTime)
//                    }, {
//                        print("Sent \(eventType.key) Event")
//                    })
//                }
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
//                    self.triggerResume(rateValue, {
//                    }, {
//                        print("Sent \(eventType.key) Event")
//                    })
//                }
//
//            } else {
//                print("play")
//                if isPlay == true {
//                    triggerResume(playHeadPos, nil, {
//                        print("Sent \(eventType.key) Event")
//                    })
//                } else {
//                    triggerResume(playHeadPos, nil, {
//                        self.isPlay = true
//                        print("Sent \(eventType.key) Event")
//                    })
//                }
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
//                    self.triggerPlaying(playHeadPos, {
//                        self.isPlaying = true
//                        DZEventCollector.shared.details.metrics.playerState = PlayerState.playing.state
//                    }, {
//                        print("Sent \(eventType.key) Event")
//                    })
//                }
//            }
            
        
        case .networkTypeChange:
            triggerNetworkTypeChange(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .networkTimed:
            triggerNetworkTimed(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .volumeChange:
            triggerVolumeChange(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .mute:
            triggerMute(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .unmute:
            triggerUnmute(nil, {
                print("Sent \(eventType.key) Event")
            })
        
        case .resize:
            triggerResize(nil, {
                print("Sent \(eventType.key) Event")
            })
        
        case .fullscreen:
            triggerFullScreen(nil, {
                print("Sent \(eventType.key) Event")
            })
                
        case .exitFullscreen:
            triggerExitFullScreen(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .audioTrackChanged:
            triggerAudioTrackChanged(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .qualityChangeRequest:
            triggerQualityChangeRequest(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .subtitleChange:
            triggerSubtitleChange(nil, {
                print("Sent \(eventType.key) Event")
            })
                
        case .castStart:
            triggerCastStart(nil, {
                print("Sent \(eventType.key) Event")
            })
        
        case .castEnd:
            triggerCastEnd(nil, {
                print("Sent \(eventType.key) Event")
            })
            
        case .playBtn:
            triggerPlayBtn(nil, {
                print("Sent \(eventType.key) Event")
            })

        // Custom Event
        case .custom(let name, let metadata):
            print("%%%% Trigger Custom Event \(name)  %%%% \n")
            DZEventCollector.shared.details.metrics.playerState = isPlaying ? PlayerState.playing.state : PlayerState.request.state
            
            print("Message \(name)_\(name)  \(Float(playHeadPos).isNaN ? 0 : Float(playHeadPos))")
            print("%%%% Trigger message for Custom Event \(name) %%%% \n")
            let event = DZEventCollector.shared.createEvent(eventType: .custom(name, metadata))
            DZEventCollector.shared.triggerMessage(event: event, rateValue: (Float(playHeadPos).isNaN ? 0 : Float(playHeadPos))) { (res) in
                print("Sent Custom Event - \(name)")
            }
            
        default:
            break;
        }
    }
    
    func triggerAdEvent(eventType: AdEventType, eventData: [String: Any]?){
        eventCommonSettings()
        
        let playHeadPos = Float(0) // (self.videoPlayerInstance != nil) ? Float(CMTimeGetSeconds(self.videoPlayerInstance.currentTime())) : Float(0)
                    
        DZEventCollector.shared.video.mediaType = .ad
        DZEventCollector.shared.adData.adId = self.adId ?? ""
        //DZEventCollector.shared.adData.adBrakeId = UUID().uuidString
        DZEventCollector.shared.adData.adSystem = self.adSystem ?? ""
        DZEventCollector.shared.adData.adPosition = self.adPosition ?? 0
        DZEventCollector.shared.video.source = self.adSourceUrl ?? ""
        DZEventCollector.shared.video.title = self.adTitle ?? ""
        //DZEventCollector.shared.video.contentType = self.adContentType ?? ""
        
        switch eventType {
        case .adError:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            errorCount += 1
            adsErrorCount += 1
            DZEventCollector.shared.details.metrics.numberErrors = errorCount
            DZEventCollector.shared.details.metrics.numberErrorsAds = adsErrorCount
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event) { (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adRequest:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            DZEventCollector.shared.adRequestTime = (CACurrentMediaTime() * 1000)
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event) { (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adLoaded:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            DZEventCollector.shared.adData.adSessionId = UserDefaults.standard.getUserAdSessionID()
            DZEventCollector.shared.numberOfAds += 1
            let mediaLoadedEvent = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: mediaLoadedEvent) { (res) in
                print("Sent \(mediaLoadedEvent.type.key) Event")
            }
            let playbackReadyEvent = DZEventCollector.shared.createAdEvent(eventType: .adPlaybackReady)
            DZEventCollector.shared.triggerMessage(event: playbackReadyEvent) { (res) in
                print("Sent \(playbackReadyEvent.type.key) Event")
            }

        case .adPlaybackReady:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event) { (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adPlay:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            DZEventCollector.shared.adStartTime = (CFAbsoluteTimeGetCurrent() * 1000)
            DZEventCollector.shared.adCompletedTime = 0
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event) { (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adPlaying:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event) { (res) in
                print("Sent \(event.type.key) Event")
            }
            
            
        case .adPlaybackStart:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            DZEventCollector.shared.adStartTime = (CACurrentMediaTime() * 1000)
            DZEventCollector.shared.adCompletedTime = 0.0
            let playbackStartEvent = DZEventCollector.shared.createAdEvent(eventType: eventType)
            playbackStartEvent.details.metrics.timeSinceLastStartedAd = 0
            DZEventCollector.shared.triggerMessage(event: playbackStartEvent) { (res) in
                print("Sent \(playbackStartEvent.type.key) Event")
            }
            
            let playingEvent = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: playingEvent) { (res) in
                print("Sent \(playingEvent.type.key) Event")
            }
            
        case .adPause:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event) { (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adResume:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let resumeEvent = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: resumeEvent) { (res) in
                print("Sent \(resumeEvent.type.key) Event")
            }
            let playingEvent = DZEventCollector.shared.createEvent(eventType: EventType.playing)
            DZEventCollector.shared.triggerMessage(event: playingEvent, rateValue: playHeadPos) { (res) in
                print("Sent \(playingEvent.type.key) Event")
            }
            
            
        case .adSkipped:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event) { (res) in
                print("Sent \(event.type.key) Event")
                DZEventCollector.shared.video.mediaType = .content
            }
            
        case .adMilestone:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            DZEventCollector.shared.adMilestoneTime = 0.0
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            event.details.metrics.timeSinceLastMilestoneAd = 0
            DZEventCollector.shared.triggerMessage(event: event, rateValue: playHeadPos){ (res) in
                print("Sent \(event.type.key) Event")
            }
            DZEventCollector.shared.adMilestoneTime = (CACurrentMediaTime() * 1000)

        case .adPlaybackComplete:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            DZEventCollector.shared.adCompletedTime = (CACurrentMediaTime() * 1000)
            DZEventCollector.shared.playbackDurationAds += Int(DZEventCollector.shared.adCompletedTime - DZEventCollector.shared.adStartTime)
            DZEventCollector.shared.adsPlayed += 1
            DZEventCollector.shared.videosPlayedCount += 1
            
            let playbackCompleteEvent = DZEventCollector.shared.createAdEvent(eventType: eventType)
            playbackCompleteEvent.details.metrics.timeSinceLastAdCompleted = 0
            UserDefaults.standard.clearUserAdSessionID()
            DZEventCollector.shared.triggerMessage(event: playbackCompleteEvent) { (res) in
                print("Sent \(playbackCompleteEvent.type.key) Event")
            }
            resetCommonAdEventData()
            
        case .adRenditionChange:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event){ (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adBreakStart:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let adBreakEvent = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: adBreakEvent){ (res) in
                print("Sent \(adBreakEvent.type.key) Event")
            }
            
            let adPlayEvent = DZEventCollector.shared.createAdEvent(eventType: AdEventType.adPlay)
            DZEventCollector.shared.triggerMessage(event: adPlayEvent){ (res) in
                print("Sent \(adPlayEvent.type.key) Event")
            }
            
            let adImpressionEvent = DZEventCollector.shared.createAdEvent(eventType: AdEventType.adImpression)
            DZEventCollector.shared.triggerMessage(event: adImpressionEvent){ (res) in
                print("Sent \(adImpressionEvent.type.key) Event")
            }
            
        case .adBreakEnd:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event){ (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adImpression:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event){ (res) in
                print("Sent \(event.type.key) Event")
            }
            
        case .adClick:
            print("%%%% Trigger \(eventType.key) Event %%%% \n")
            let event = DZEventCollector.shared.createAdEvent(eventType: eventType)
            DZEventCollector.shared.triggerMessage(event: event){ (res) in
                print("Sent \(event.type.key) Event")
            }
     
        default:
            break;
        }
    }
    
    //MARK:- CUSTOM EVENTS
    func triggerCustomEvent(name: String?, metadata:[String:Any]?) {
        if name != nil && name != "" {
            print("%%%% Trigger Custom Event - \(String(describing: name)) %%%% \n")
            triggerEvent(eventType: .custom(name!, metadata))
        }
    }
}
    
extension DZCollectoriOSBitmovin {
    // Handle AVPlayerAccess - get playerBitRate & absShift (rendition change)
    @objc func handleAVPlayerAccess(notification: Notification) {
        guard let playerItem = notification.object as? AVPlayerItem,
            let lastEvent = playerItem.accessLog()?.events.last else { return }
        
        guard let track = playerItem.asset.tracks(withMediaType: AVMediaType.video).first else { return }
        let size = track.naturalSize.applying(track.preferredTransform)
        self.renditionHeight = abs(Int(size.height))
        self.renditionWidth = abs(Int(size.width))
        
        let indicatedBitrate = lastEvent.indicatedBitrate
        let oldValue = (self.playerBitRate ?? 0)
        let diffBitRate = (oldValue == 0) ? indicatedBitrate : indicatedBitrate - oldValue
        self.playerBitRate = indicatedBitrate
        let absShiftInt = Int(diffBitRate.signum())
        self.absShift = (absShiftInt > 0) ? "up" : ((absShiftInt < 0) ? "down" : "")
        
        let numberOfBitsTransferred = Double(lastEvent.numberOfBytesTransferred * 8)
        let videoBitrate = (lastEvent.segmentsDownloadedDuration != 0) ? (numberOfBitsTransferred / lastEvent.segmentsDownloadedDuration) : 0
        self.renditionVideoBitrate = videoBitrate / 1000
        
        if(self.absShift != ""){
            triggerEvent(eventType: .renditionChange)
        }
    }
    
    
    //MARK:- Player Did FinishPlaying
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Playback Complete")
        isComplete = true
        
        DZEventCollector.shared.details.metrics.playerState = PlayerState.completed.state
        
        DZEventCollector.shared.videosPlayedCount += 1
        stopped = true
        isPlaying = false
        isPlay = false
        presentTime = 0.0
        prevTime = 0.0
        loopIndex = loopIndex + 1
        stopMilestoneObserver()
//        removePeriodicTimeObserver()
        
#if canImport(GoogleInteractiveMediaAds)
        adsLoader.contentComplete()
#endif
        triggerEvent(eventType: .playbackComplete)
        
        stallCount = 0
    }
    
    //MARK :- Periodic and boundary observers.
    func addPeriodicTimeObserver() {
        let intervalVal = DZWebBroker.shared.interval
        let intervalValDouble: Double = Double(intervalVal/1000)
        let interval = CMTimeMakeWithSeconds(intervalValDouble, preferredTimescale: Int32(intervalValDouble))
        
        if DZWebBroker.shared.interval > 0 {
//            periodicObserverToken = videoPlayerInstance.addPeriodicTimeObserver(forInterval: interval, queue: .main, using: { [weak self] (time) in
//                guard let self = self else { return }
//
//                if self.isPlaying {
//                    DZEventCollector.shared.details.metrics.playerState = PlayerState.playing.state
//                } else {
//                    DZEventCollector.shared.details.metrics.playerState = PlayerState.paused.state
//                }
//
//                self.triggerEvent(eventType: .heartbeat)
//            }) as AnyObject
        } else {
            print("No Flux Data %%%% \n")
        }
    }
    
    func keyRateChanged(change: [NSKeyValueChangeKey : Any]?) {
//        var playHeadPos = Float(CMTimeGetSeconds(self.videoPlayerInstance.currentTime()))
//        let playerItem = self.videoPlayerInstance.currentItem
//        let urlAsset = playerItem?.asset as? AVURLAsset
//        let duration = urlAsset?.duration ?? CMTimeMake(value: 99, timescale: 1)
//
//        let rate = change?[NSKeyValueChangeKey.newKey] as? Float
//        DZEventCollector.shared.details.metrics.playbackRate = rate!
//        switch rate {
//
//        case 0.0?:
//            print("pause")
//            removePeriodicTimeObserver()
//            stopped = true
//            isPlaying = false
//            prevTime = self.videoPlayerInstance.currentTime().seconds
//            DZEventCollector.shared.details.metrics.playerState = PlayerState.paused.state
//
//            if isComplete == false  && duration > videoPlayerInstance.currentTime() {
//                print("%%%% Trigger \(EventType.pause.key) Event %%%% \n")
//                triggerEvent(eventType: .pause, rateValue: rate!)
//            }
//
//        case 1.0?:
//            print("stopped :: \(stopped)")
//            if stopped {
//                stopped = false
//                if playHeadPos == 0.0 {
//                    playHeadPos = Float(CMTimeGetSeconds(videoPlayerInstance.currentTime()))
//                    self.startTime = CACurrentMediaTime() * 1000
//                }
//                triggerEvent(eventType: .resume)
//            } else {
//                if playHeadPos == 0.0 {
//                    playHeadPos = Float(CMTimeGetSeconds(self.videoPlayerInstance.currentTime()))
//                }
//                print("play")
//                var eventType = EventType.none
//                if isPlay == true {
//                    eventType = EventType.resume
//                    if self.isBuffering == true {
//                        triggerEvent(eventType: .bufferEnd)
//                        self.isBuffering = false
////                        self.bufferDuration += ((CACurrentMediaTime() * 1000) - self.bufferTime)
//                    }
//                } else {
//                    eventType = EventType.play
//                    isPlay = true
//                }
//
//                isPlaying = true
//                DZEventCollector.shared.details.metrics.playerState = PlayerState.playing.state
//                print("%%%% Trigger \(eventType.key) Event %%%% \n")
//                triggerEvent(eventType: eventType)
//
//                print("%%%% Trigger \(EventType.playing.key) Event %%%% \n")
//                triggerEvent(eventType: .playing)
//            }
//            break
//        default:
//            print("")
//        }
    }

    func keyStatusChanged() {
//        let playerItem = self.videoPlayerInstance.currentItem
//        let urlAsset = playerItem?.asset as? AVURLAsset
//
//        let status: AVPlayer.Status = self.videoPlayerInstance.status
//
//        if(status == AVPlayer.Status.readyToPlay){
//            print("READY_TO_PLAY")
//            print("PLAYBACK_START")
//            self.startTime = CACurrentMediaTime() * 1000
//            self.timeSinceStarted = 0
//
//            isPlay = true
//            isPlaying = true
//            cdn = (urlAsset?.url.host ?? "") as String
//
//        #if canImport(GoogleInteractiveMediaAds)
//            if !adsRequested {
//                if let _ = adTagURLString {
//                    adsRequested = true
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//                        self.requestAds()
//                    }
//                }
//            }
//        #endif
//        } else {
//            if (status == AVPlayer.Status.unknown){
//                let errorLocalDesc = self.videoPlayerInstance.currentItem?.error?.localizedDescription
//                errorCode = self.videoPlayerInstance.error?.localizedDescription ?? ""
//                errorMsg = errorLocalDesc ?? ""
//                triggerEvent(eventType: .error)
//            }
//        }
    }
    
    func keyItemSatusChanged(change: [NSKeyValueChangeKey : Any]?) {
//        //let playHeadPos = Float(CMTimeGetSeconds(self.videoPlayerInstance.currentTime()))
//
//        let status: AVPlayerItem.Status
//        if let statusNumber = change?[.newKey] as? NSNumber {
//            status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
//        } else {
//            status = .unknown
//        }
//        switch status {
//        case .readyToPlay:
//            timeSinceStarted = 0
//            triggerEvent(eventType: .playbackReady)
//
//        case .failed:
//            let errorLocalDesc = self.videoPlayerInstance.currentItem?.error?.localizedDescription
//            errorCode = self.videoPlayerInstance.error?.localizedDescription ?? ""
//            errorMsg = errorLocalDesc ?? ""
//            triggerEvent(eventType: .error)
//
//        case .unknown:
//            print("Item unkown")
//
//        @unknown default:
//            print("Unkown item")
//        }
    }
    
    
    //MARK:- Method which listens to events, can be used as a trigger point for webservice initiation.
    @objc open override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
//        guard let playerItem = videoPlayerInstance.currentItem else {
//            return
//        }
//
//        if (playerItem.isPlaybackLikelyToKeepUp) {
//            if self.contentStalled == true {
//                triggerEvent(eventType: .stallEnd)
//                self.contentStalled = false
//                self.stallAdd = true
//            }
//        }
//        else if (playerItem.isPlaybackBufferEmpty) {
//            if self.isBuffering == true {
//                triggerEvent(eventType: .stallStart)
//                self.contentStalled = true
//
//                self.isBuffering = false
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                    self.triggerEvent(eventType: .bufferEnd)
//                }
//
//                if self.contentStalled == true {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
//                        self.triggerEvent(eventType: .stallEnd)
//                    }
//                    self.contentStalled = false
//                    self.stallAdd = true
//                }
//            }
//            else {
//                DZEventCollector.shared.details.metrics.playerState = PlayerState.buffering.state
//                self.isBuffering = true
//                triggerEvent(eventType: .bufferStart)
//            }
//        }
//        else if (playerItem.isPlaybackBufferFull) {
//            print("Buffer full - hide loader")
//            if isPlaybackStart == false {
//                print("Buffer full - isPlaybackStart")
//                isPlaybackStart = true
//            }
//            self.isBuffering = false
//            triggerEvent(eventType: .bufferEnd)
//        }
//        else {
//            DZEventCollector.shared.details.metrics.playerState = PlayerState.buffering.state
//            if self.bufferStartSent == false && self.isBuffering == false {}
//        }
//
//        switch keyPath {
//        case "rate"?:
//            keyRateChanged(change: change)
//
//        case "status"?:
//            keyStatusChanged()
//
//        case "itemStatus"?:
//            keyItemSatusChanged(change:change)
//
//        case "timebase"?:
//            let stringTB = videoPlayerInstance.currentItem?.timebase
//            //let flt = CMTimebaseGetRate((self.videoPlayerInstance.currentItem?.timebase)!)
//            print("stringTB -----  \(String(describing: stringTB))")
//
//        case "tracks"?:
//            for singleTerm in (self.videoPlayerInstance.currentItem?.asset.tracks)!{
//                print("singleTerm -----  \(singleTerm)")
//            }
//
//        case "seekableTimeRanges"?:
//            let seekableTimeRanges = self.videoPlayerInstance.currentItem?.seekableTimeRanges
//            if seekableTimeRanges?.count ?? 0 > 0 {
//                let range = seekableTimeRanges![0]
//                let timeRange = range.timeRangeValue
//                let startSeconds = CMTimeGetSeconds(timeRange.start)
//                let durationSeconds = CMTimeGetSeconds(timeRange.duration)
//                let xminimumValue = Float(startSeconds)
//                let xmaximumValue = Float(startSeconds + durationSeconds)
//                print("xminimumValue -----  \(xminimumValue)")
//                print("xmaximumValue -----  \(xmaximumValue)")
//            }
//
//        case "playbackBufferEmpty"?:
//            print("Buffer empty")
//
//
//        case "playbackBufferFull"?:
//            if self.contentStalled == true {
//            }
//
//            DZEventCollector.shared.details.metrics.playerState = PlayerState.buffering.state
//            self.isBuffering = false
//            triggerEvent(eventType: .bufferEnd)
//            self.stallAdd = true
//
//        case "playbackStalled"?:
//            print("PLAYBACK STALL")
//            if self.contentStalled == true {
//                triggerEvent(eventType: .stallEnd)
//                self.contentStalled = false
//                self.stallAdd = true
//            } else {
//                triggerEvent(eventType: .stallStart)
//                self.contentStalled = true
//            }
//            /*case "AVPlayerItemNewErrorLogEntryNotification"?:
//             let lastEvent = self.videoPlayerInstance.currentItem?.accessLog()?.events.last
//             let indicatedBitrate = lastEvent?.indicatedBitrate*/
//
//        case "volume"?:
//            print ("volume")
//
//        case "timedMetadata"?:
//            print ("timed metadata")
////            for metadata in (self.videoPlayerInstance.currentItem?.asset.metadata)!{
////            }
//
//        default:
//            print("")
//        }
    }
    
    
    //MARK :- Periodic and boundary observers.
    func addMilestoneTimeObserver() {
        print("%%%% Trigger \(EventType.milestone.key) Event %%%% \n")
        self.milestoneTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.triggerMilestoneEvent), userInfo: nil, repeats: true)
    }
    
    func stopMilestoneObserver() {
        self.milestoneTimer!.invalidate()
    }
    
    @objc func triggerMilestoneEvent() {
//        if isPlaying == true {
//            let playHeadPos = Float(CMTimeGetSeconds(self.videoPlayerInstance.currentTime()))
//            let playerItem = self.videoPlayerInstance.currentItem
//            let urlAsset = playerItem?.asset as? AVURLAsset
//            let duration = urlAsset?.duration ?? CMTimeMake(value: 99, timescale: 1)
//
//            let intervalVal = DZWebBroker.shared.interval
//            let intervalValInt:Int = Int(intervalVal/1000)
//            let playHeadPositionInt = Int(playHeadPos)
//
//            let milestone10 = (Float(CMTimeGetSeconds(duration)) * 0.1)
//            let milestone25 = (Float(CMTimeGetSeconds(duration)) * 0.25)
//            let milestone50 = (Float(CMTimeGetSeconds(duration)) * 0.50)
//            let milestone75 = (Float(CMTimeGetSeconds(duration)) * 0.75)
//            let milestone90 = (Float(CMTimeGetSeconds(duration)) * 0.90)
//            let milestone95 = (Float(CMTimeGetSeconds(duration)) * 0.95)
//
//            var et = EventType.none
//            if playHeadPos.rounded() == milestone10.rounded() {
//                et = EventType.milestone
//                self.milestonePercent = 0.10
//                self.milestoneTime = CACurrentMediaTime()*1000
//            } else if playHeadPos.rounded() == milestone25.rounded() {
//                et = EventType.milestone
//                self.milestonePercent = 0.25
//                self.milestoneTime = CACurrentMediaTime()*1000
//            } else if playHeadPos.rounded() == milestone50.rounded() {
//                et = EventType.milestone
//                self.milestonePercent = 0.50
//                self.milestoneTime = CACurrentMediaTime()*1000
//            } else if playHeadPos.rounded() == milestone75.rounded() {
//                et = EventType.milestone
//                self.milestonePercent = 0.75
//                self.milestoneTime = CACurrentMediaTime()*1000
//            } else if playHeadPos.rounded() == milestone90.rounded() {
//                et = EventType.milestone
//                self.milestonePercent = 0.90
//                self.milestoneTime = CACurrentMediaTime()*1000
//            } else if playHeadPos.rounded() == milestone95.rounded() {
//                et = EventType.milestone
//                self.milestonePercent = 0.95
//                self.milestoneTime = CACurrentMediaTime()*1000
//            } else if playHeadPositionInt != 0 && intervalValInt != 0  {
//                if(playHeadPositionInt % intervalValInt == 0){
//                    et = EventType.networkTimed
//                }
//            }
//            
//            print("Milestone percent \(self.milestonePercent)")
//            
//            if et != .none {
//                print("Milestone Event ::\(et.key)")
//                DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
//                    DZEventCollector.shared.details.metrics.playerState = PlayerState.playing.state
//                    self.triggerEvent(eventType: et)
//                    self.heartbeatTime = CACurrentMediaTime()*1000
//                }
//            }
//            else {
//                print("Not a milestone")
//            }
//        }
    }
    
    func addCMCDToVideoUrl(videoUrl: String) -> String {
        let sid = "sid=\"\(UserDefaults.standard.getUserSessionID())\""
        let rid = "rid=\"\(UserDefaults.standard.getUserRequestID())\""
        let encodedSid = sid.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        let encodedRid = rid.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
        
        let cmcd = "CMCD=\(encodedRid!),\(encodedSid!)"
        
        var withCMCD = ""
        if videoUrl.contains("?") {
            withCMCD = videoUrl.replacingOccurrences(of: "?", with: "?\(cmcd)&")
        }
        else {
            withCMCD = "\(videoUrl)?\(cmcd)"
            
        }
        return withCMCD
    }
}


// Ads
extension DZCollectoriOSBitmovin {
#if canImport(GoogleInteractiveMediaAds)
    @objc open func requestAds() {
        // Create ad display container for ad rendering.
        if let controllerInstance = videoPlayerControllerInstance {
            adDisplayContainer = IMAAdDisplayContainer(adContainer: controllerInstance.view, viewController: controllerInstance)
            // Create an ad request with our ad tag, display container, and optional user context.
            let request = IMAAdsRequest(
              adTagUrl: adTagURLString,
              adDisplayContainer: adDisplayContainer,
              contentPlayhead: contentPlayhead,
              userContext: nil)
            
            adsLoader.requestAds(with: request)
            triggerAdEvent(eventType: .adRequest, eventData:nil)
        }
    }
#endif
}

// MARK: - IMAAdsLoaderDelegate
#if canImport(GoogleInteractiveMediaAds)
extension DZCollectoriOSBitmovin: IMAAdsLoaderDelegate {
    public func adsLoader(_ loader: IMAAdsLoader!, adsLoadedWith adsLoadedData: IMAAdsLoadedData!) {
        adsManager = adsLoadedData.adsManager
        adsManager.delegate = self
        adsManager.initialize(with: nil)
      }

    public func adsLoader(_ loader: IMAAdsLoader!, failedWith adErrorData: IMAAdLoadingErrorData!) {
        print("Error loading ads: " + adErrorData.adError.message)
        videoPlayerControllerInstance?.player?.play()
      }
}
#endif

// MARK: - IMAAdsManagerDelegate
#if canImport(GoogleInteractiveMediaAds)
extension DZCollectoriOSBitmovin: IMAAdsManagerDelegate {
    public func adsManager(_ adsManager: IMAAdsManager!, didReceive event: IMAAdEvent!) {
        let ad = event.ad
        let adData = event.adData
        
        switch event.type {
        case .AD_BREAK_READY:
            print("$$$$$$$$$ Ad break ready")
            
        case .AD_BREAK_FETCH_ERROR:
            print("$$$$$$$$$ Ad break fetch error")
            
        case .AD_BREAK_STARTED:
            print("$$$$$$$$$ Ad break started")
            
        case .AD_BREAK_ENDED:
            print("$$$$$$$$$ Ad break ended")
            
        case .AD_PERIOD_STARTED:
            print("$$$$$$$$$ Ad period started")
            
        case .AD_PERIOD_ENDED:
            print("$$$$$$$$$ Ad period ended")
            
        case .ALL_ADS_COMPLETED:
            print("$$$$$$$$$ All ads completed")
            
            
        case .LOG:
            print("$$$$$$$$$ Ad Log")
            
        case .STREAM_LOADED:
            print("$$$$$$$$$ Ad Stream loaded")
            triggerAdEvent(eventType: .adLoaded, eventData: adData)
            
        case .STREAM_STARTED:
            print("$$$$$$$$$ Ad Stream started")
            triggerAdEvent(eventType: .adBreakStart, eventData: adData)
//            triggerAdEvent(eventType: .adPlay, eventData: adData)
//            triggerAdEvent(eventType: .adImpression, eventData: adData)

        case .LOADED:
            print("$$$$$$$$$ Ad Loaded")
            self.adId = ad?.adId
            self.adTitle = ad?.adTitle
            self.adContentType = ad?.contentType
            
            self.adSystem = ad?.adSystem
//            self.adWrapper = ad?.adWrapper
            self.adPosition = ad?.adPodInfo.podIndex
            
//            let width = ad?.vastMediaWidth
//            let height = ad?.vastMediaHeight
//            let bitrate = ad?.vastMediaBitrate
            
            triggerAdEvent(eventType: .adLoaded, eventData: adData)
            adsManager.start()
            
        case .ICON_FALLBACK_IMAGE_CLOSED:
            print("$$$$$$$$$ Ad Icon Fallback image closed")
          // Resume playback after the user has closed the dialog.
            adsManager.resume()

        case .STARTED:
            print("$$$$$$$$$ Ad Started")
            triggerAdEvent(eventType: .adPlaybackStart, eventData: adData)
            
        case .SKIPPED:
            print("$$$$$$$$$ Ad Skipped")
            triggerAdEvent(eventType: .adSkipped, eventData: adData)

            
        case .PAUSE:
            print("$$$$$$$$$ Ad Pause")
            triggerAdEvent(eventType: .adPause, eventData: adData)
            
        case .RESUME:
            print("$$$$$$$$$ Ad Resume")
            triggerAdEvent(eventType: .adResume, eventData: adData)
            
        case .COMPLETE:
            print("$$$$$$$$$ Ad Complete")
            triggerAdEvent(eventType: .adPlaybackComplete, eventData: adData)
            
        case .FIRST_QUARTILE:
            print("$$$$$$$$$ Ad Milestone 25%")
            DZEventCollector.shared.details.attributes.milestonePercent = 0.25
            self.milestonePercent = 0.25
            triggerAdEvent(eventType: .adMilestone, eventData: adData)
            
        case .MIDPOINT:
            print("$$$$$$$$$ Ad Milestone 50%")
            DZEventCollector.shared.details.attributes.milestonePercent = 0.50
            self.milestonePercent = 0.50
            triggerAdEvent(eventType: .adMilestone, eventData: adData)
            
        case .THIRD_QUARTILE:
            print("$$$$$$$$$ Ad Milestone 75%")
            DZEventCollector.shared.details.attributes.milestonePercent = 0.75
            self.milestonePercent = 0.75
            triggerAdEvent(eventType: .adMilestone, eventData: adData)
            
            
        case .CUEPOINTS_CHANGED:
            print("$$$$$$$$$ Ad Cuepoint changed")
            print("Cuepoint changed")
            
        case .ICON_TAPPED:
            print("$$$$$$$$$ Ad Icon tapped")
            print("Icon tapped")

        case .CLICKED:
            print("$$$$$$$$$ Ad Clicked")
            triggerAdEvent(eventType: .adClick, eventData: adData)
            
        case .TAPPED:
            print("$$$$$$$$$ Ad Taped")
            triggerAdEvent(eventType: .adClick, eventData: adData)
            
        @unknown default:
            print("$$$$$$$$$ Ad Unknown case")
        }
    }
    
    public func adsManager(_ adsManager: IMAAdsManager!, didReceive error: IMAAdError!) {
        // Fall back to playing content
        print("$$$$$$$$$ Ad Error \(error.message) (ads manager)")
        //print("%%%% AdsManager error: " + error.message)
        errorCode = String(error.code.rawValue)
        errorMsg = error.message
        
        triggerAdEvent(eventType: .adError, eventData: nil)
        
        videoPlayerControllerInstance?.player?.play()
    }
    
    public func adsManagerDidRequestContentPause(_ adsManager: IMAAdsManager!) {
        videoPlayerControllerInstance?.player?.pause()
        adBreakActive = true
        videoPlayerControllerInstance?.setNeedsFocusUpdate()
    }

    public func adsManagerDidRequestContentResume(_ adsManager: IMAAdsManager!) {
        // Resume the content since the SDK is done playing ads (at least for now).
        videoPlayerControllerInstance?.player?.play()
        adBreakActive = false
        videoPlayerControllerInstance?.setNeedsFocusUpdate()
    }
    
    // here calculate milestones that missing (0.1, 0.90, 0.95)
    public func adsManager(_ adsManager: IMAAdsManager!, adDidProgressToTime mediaTime: TimeInterval, totalTime: TimeInterval) {
        let ratio = Int ( 1000 * (mediaTime / totalTime))
        
        switch ratio {
        case 90...110 :
            print("$$$$$$$$$ Ad Milestone 10%")
            DZEventCollector.shared.details.attributes.milestonePercent = 0.10
            self.milestonePercent = 0.10
//            self.adMilestoneTime = CACurrentMediaTime()*1000
            triggerAdEvent(eventType: .adMilestone, eventData: nil)
            
        case 890...910:
            print("$$$$$$$$$ Ad Milestone 90%")
            DZEventCollector.shared.details.attributes.milestonePercent = 0.90
            self.milestonePercent = 0.90
//            self.adMilestoneTime = CACurrentMediaTime()*1000
            triggerAdEvent(eventType: .adMilestone, eventData: nil)
            
        case 940...965:
            print("$$$$$$$$$ Ad Milestone 95%")
            DZEventCollector.shared.details.attributes.milestonePercent = 0.95
            self.milestonePercent = 0.95
//            self.adMilestoneTime = CACurrentMediaTime()*1000
            triggerAdEvent(eventType: .adMilestone, eventData: nil)
            
        default:
            break
        }
    }
    
    public func adsManager(_ adsManager: IMAAdsManager!, adDidBufferToMediaTime mediaTime: TimeInterval) {
        print("$$$$$$$$$ Ad Did buffer to media time (ads manager): time \(mediaTime)")
    }
    
    public func adsManagerAdPlaybackReady(_ adsManager: IMAAdsManager!) {
        // playback keep up
        print("$$$$$$$$$ Ad Playback ready (ads manager)") // buffer end?
    }
    
    public func adsManagerAdDidStartBuffering(_ adsManager: IMAAdsManager!) {
        // stalling
        print("$$$$$$$$$ Ad Stalling ((ads manager)")
    }
}

#endif
